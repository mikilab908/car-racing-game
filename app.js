$(function () {
  $('.race-btn').on('click', function () {
    let countDown = 3;

    $('.race-btn').attr('disabled', 'disabled');
    $('.restart-btn').attr('disabled', 'disabled');

    $('#count-down').addClass('count-down');
    $('.count-down').html(countDown);

    let interval = setInterval(function () {
      $('.race-btn').attr('disabled', 'disabled');
      $('.restart-btn').attr('disabled', 'disabled');

      countDown--;

      $('.count-down').html(countDown);

      if (countDown == 0) {
        $('.count-down').html('');
        clearInterval(interval);

        $('#count-down').removeClass('count-down');

        $('.race-btn').attr('disabled', 'disabled');
        $('.restart-btn').attr('disabled', 'disabled');

        let randomTimeCar1 = Math.floor(Math.random() * (1800 + 1) + 300);
        let randomTimeCar2 = Math.floor(Math.random() * (1800 + 1) + 300);

        let first = true;

        let animateCar = (raceCar, time) => {
          $(raceCar).animate({ marginLeft: '83%' }, time, function () {
            let generateRow = (score, color, time) => {
              $('.flag').animate({ opacity: 1 }, 500);

              score.append(`<tr>
                              <td>
                                <h5>
                                  Finished in:
                                  <span class=${color}>${
                first ? 'first' : 'second'
              }</span>
                                  place with a time of:
                                  <span class=${color}>${time}</span>
                                  milliseconds!
                                </h5>
                              </td>
                            </tr>`);

              $('body').on('click', function () {
                $('.flag').css({ opacity: 0 });
                $('.restart-btn').removeAttr('disabled');
              });
            };

            if (randomTimeCar1 < randomTimeCar2) {
              if (first == true) {
                generateRow($('.scoreCar1'), 'car1-color', randomTimeCar1);

                localStorage.setItem('Car1', randomTimeCar1);
                first = false;
              } else {
                generateRow($('.scoreCar2'), 'car2-color', randomTimeCar2);

                localStorage.setItem('Car2', randomTimeCar2);
                first = true;
              }
            } else {
              if (first == true) {
                generateRow($('.scoreCar2'), 'car2-color', randomTimeCar2);

                localStorage.setItem('Car2', randomTimeCar2);
                first = false;
              } else {
                generateRow($('.scoreCar1'), 'car1-color', randomTimeCar1);

                localStorage.setItem('Car1', randomTimeCar1);
                first = true;
              }
            }
          });
        };

        animateCar($('.car1'), randomTimeCar1);
        animateCar($('.car2'), randomTimeCar2);
      }
    }, 1000);
  });

  $('.restart-btn').on('click', function () {
    $('.car1').css({ marginLeft: '0' });
    $('.car2').css({ marginLeft: '0' });
    $('.race-btn').removeAttr('disabled');
  });

  $(function () {
    let car1Time = localStorage.getItem('Car1');
    let car2Time = localStorage.getItem('Car2');

    let generateResultTable = (time, color, car) => {
      $('.lastScore').append(`<tr>
                                <td class="previous">
                                  <h5>
                                    <span class=${color}>${car}</span> finished in
                                    <span class=${color}>${
        car1Time < car2Time ? 'first' : 'second'
      }</span> place, with a time of
                                    <span class=${color}>${time}</span> milliseconds!
                                  </h5>
                                </td>
                              </tr>`);
    };

    if (car1Time == null && car2Time == null) {
      $('.previousRezult').hide();
      $('.lastScore').hide();
    } else {
      $('.previousRezult').show();
      if (parseInt(car1Time) < parseInt(car2Time)) {
        generateResultTable(car1Time, 'car1-color', 'Car1');
        generateResultTable(car2Time, 'car2-color', 'Car2');
      } else {
        generateResultTable(car1Time, 'car1-color', 'Car1');
        generateResultTable(car2Time, 'car2-color', 'Car2');
      }
    }
  });
});
